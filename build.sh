#!/bin/bash -ex

# CONFIG
prefix="MacVim"
suffix=""
munki_package_name="MacVim"
display_name="MacVim"
category="Utilities"
description="Vim is an almost compatible version of the UNIX editor Vi.  Many new features
have been added: multi-level undo, syntax highlighting, command line history,
on-line help, spell checking, filename completion, block operations, etc.
There is also a Graphical User Interface (GUI) available.  See
runtime/doc/vi_diff.txt for differences with Vi."
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

# Obtain version info
version1=`/usr/libexec/PlistBuddy -c "print :CFBundleShortVersionString" "${mountpoint}/MacVim.app/Contents/Info.plist"`
version2=`/usr/libexec/PlistBuddy -c "print :CFBundleVersion" "${mountpoint}/MacVim.app/Contents/Info.plist"`
version=${version1}.${version2}

mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
#/usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg
mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R $app_in_dmg build-root/Applications

# (cd build-root; pax -rz -f ../pkg/*/Payload)


hdiutil detach "${mountpoint}"

#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist

plutil -replace BundleIsRelocatable -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} app.pkg

rm -rf Component-${munki_package_name}.plist

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg ${key_files} | /bin/bash > app.plist

perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

/usr/libexec/PlistBuddy -c "Set installs:0:version_comparison_key CFBundleVersion" "${plist}"

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.12.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" description -string "${description}"
defaults write "${plist}" category -string "${category}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"


# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
